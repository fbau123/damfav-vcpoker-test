package com.damfav.vcpoker.test.model.ai;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import android.util.Log;

import com.damfav.vcpoker.model.ai.EffectiveHandCalculator;
import com.damfav.vcpoker.model.hand.Card;
import com.damfav.vcpoker.model.hand.Hand;
import com.damfav.vcpoker.model.hand.HandFacade;

public class EffectiveHandCalculatorTest extends AndroidTestCase{
	
	private EffectiveHandCalculator ehsCalc = new EffectiveHandCalculator();
	
	public void testCalculate(){
		
		List<Card> botCards = new ArrayList<>();
		botCards.add(new Card(1,1));
		botCards.add(new Card(1,2));
		
		List<Card> boardCards = new ArrayList<>();
		boardCards.add(new Card(1, 3));
		boardCards.add(new Card(1, 4));
		boardCards.add(new Card(4, 2));
		boardCards.add(new Card(3, 4));
		boardCards.add(new Card(9, 4));
		
		List<Card> totalCards = new ArrayList<>(botCards);
		totalCards.addAll(boardCards);
		
		Hand botHand = HandFacade.getHand(totalCards); 
		
		
		
		double ehs = ehsCalc.calculate(botHand, boardCards);
		Log.d("EHS", String.valueOf(ehs));
		System.out.println(String.valueOf(ehs));
		
		
		Assert.assertTrue(ehs > 0.5);
		
		
		
	}
	
	
	public void testCreatePossibleCards(){
		List<Card> cardsTaken = new ArrayList<>();
		cardsTaken.add(new Card(1,1));
		cardsTaken.add(new Card(1,2));
		cardsTaken.add(new Card(1,3));
		cardsTaken.add(new Card(2,1));
		cardsTaken.add(new Card(3,1));
		cardsTaken.add(new Card(4,1));
		cardsTaken.add(new Card(4,2));
		
		boolean[] poss = ehsCalc.createPossibleCards(cardsTaken);
		
		if (poss[0]){
			Assert.fail();
		}
		
		if (poss[13]){
			Assert.fail();
		}
		
		if (poss[26]){
			Assert.fail();
		}
		
		
	}
}
